// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Veles.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Veles, "Veles" );

DEFINE_LOG_CATEGORY(LogVeles)
 