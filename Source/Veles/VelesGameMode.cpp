// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "VelesGameMode.h"
#include "VelesPlayerController.h"
#include "VelesCharacter.h"
#include "UObject/ConstructorHelpers.h"

AVelesGameMode::AVelesGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AVelesPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}